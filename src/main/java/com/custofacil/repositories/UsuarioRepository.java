package com.custofacil.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.custofacil.domain.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
	
	Optional<Usuario> findByUsername(String username);

}

package com.custofacil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.custofacil.domain.Motorista;

public interface MotoristaRepository extends JpaRepository<Motorista, Integer> {

}

package com.custofacil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.custofacil.domain.Custo;

public interface CustoRepository extends JpaRepository<Custo, Integer> {

}

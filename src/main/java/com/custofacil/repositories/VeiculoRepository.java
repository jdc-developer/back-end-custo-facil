package com.custofacil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.custofacil.domain.Veiculo;

public interface VeiculoRepository extends JpaRepository<Veiculo, Integer> {

}

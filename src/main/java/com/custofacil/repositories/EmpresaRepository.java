package com.custofacil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.custofacil.domain.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Integer> {

}

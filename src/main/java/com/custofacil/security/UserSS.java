package com.custofacil.security;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.custofacil.domain.enums.UserRole;

public class UserSS implements UserDetails {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String cpf;
	private String senha;
	private Collection<? extends GrantedAuthority> authorities;
	
	public Integer getId() {
		return id;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return senha;
	}

	@Override
	public String getUsername() {
		return cpf;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public UserSS(Integer id, String cpf, String senha, UserRole role) {
		super();
		this.id = id;
		this.cpf = cpf;
		this.senha = senha;
		this.authorities = Arrays.asList(new SimpleGrantedAuthority(role.getDescricao()));
	}
	
	public boolean hasRole(UserRole role) {
		return getAuthorities().contains(new SimpleGrantedAuthority(role.getDescricao()));
	}

	public UserSS() {
		super();
	}
}

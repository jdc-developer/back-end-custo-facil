package com.custofacil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackEndCustoFacilApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackEndCustoFacilApplication.class, args);
	}

}

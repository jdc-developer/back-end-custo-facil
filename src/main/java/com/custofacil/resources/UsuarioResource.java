package com.custofacil.resources;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.custofacil.domain.Usuario;
import com.custofacil.dto.UsuarioCreateDTO;
import com.custofacil.dto.UsuarioUpdateDTO;
import com.custofacil.services.UsuarioService;

@Controller
@RequestMapping(value = "/usuarios")
public class UsuarioResource {
	
	@Autowired
	private UsuarioService service;
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<Usuario> find(@PathVariable Integer id) {
		Usuario obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(value = "/findByToken", method = RequestMethod.GET)
	public ResponseEntity<Usuario> findByToken() {
		Usuario obj = service.findByToken();
		return ResponseEntity.ok().body(obj);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody UsuarioCreateDTO objDto) {
		return ResponseEntity.created(null).build();
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Usuario> update(@PathVariable Integer id, @Valid @RequestBody UsuarioUpdateDTO objDto) {
		return ResponseEntity.ok().body(null);
	}

}

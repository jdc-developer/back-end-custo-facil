package com.custofacil.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.custofacil.domain.Usuario;
import com.custofacil.repositories.UsuarioRepository;
import com.custofacil.security.UserSS;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioRepository rep;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Usuario> usuarioOpt = rep.findByUsername(username);
		if (!usuarioOpt.isPresent()) {
			throw new UsernameNotFoundException(username);
		}
		Usuario usuario = usuarioOpt.get();
		return new UserSS(usuario.getId(), usuario.getUsername(), usuario.getSenha(), usuario.getRoleEnum());
	}

}

package com.custofacil.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.custofacil.domain.Empresa;
import com.custofacil.domain.Usuario;
import com.custofacil.domain.enums.UserRole;
import com.custofacil.repositories.UsuarioRepository;
import com.custofacil.security.UserSS;
import com.custofacil.services.exception.AuthorizationException;
import com.custofacil.services.exception.ObjectNotFoundException;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository repository;
	
	public Usuario find(Integer id) {
		Optional<Usuario> objOpt = repository.findById(id);
		UserSS user = UserService.authenticated();

		objOpt.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Usuario.class.getName()));

		Usuario objSearch = objOpt.get();

		Empresa empresaUser = repository.findById(user.getId()).get().getEmpresa();
		Empresa empresaSearch = objSearch.getEmpresa();

		if (user == null || !user.hasRole(UserRole.ADMIN) && !empresaUser.getId().equals(empresaSearch.getId())) {
			throw new AuthorizationException("Acesso negado");
		}

		return objSearch;
	}
	
	public Usuario findByToken() {
		UserSS user = UserService.authenticated();
		Optional<Usuario> objOpt = repository.findById(user.getId());

		objOpt.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + user.getId() + ", Tipo: " + Usuario.class.getName()));

		Usuario objSearch = objOpt.get();
		if (objSearch.getId() != user.getId())
			throw new AuthorizationException("Acesso negado");

		return objSearch;
	}
	
	public Usuario insert(Usuario obj) {
		obj.setId(null);
		return repository.save(obj);
	}
	
	public Usuario update(Usuario obj) {
		Usuario newObj = find(obj.getId());
		updateData(newObj, newObj);
		return repository.save(obj);
	}
	
	public void delete(Integer id) {
		repository.deleteById(id);
	}
	
	private void updateData(Usuario newObj, Usuario obj) {
		newObj.setNome(obj.getNome());
		newObj.setUsername(obj.getUsername());
		newObj.setRole(obj.getRoleEnum());
		newObj.setSenha(obj.getSenha());
	}

}

package com.custofacil.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class AuthDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Nome de usuário obrigatório")
	@Size(min = 4, message = "O nome de usuário deve ter pelo menos 4 caracteres")
	private String username;

	@NotBlank(message = "Senha obrigatória")
	@Size(min = 4, message = "A senha deve ter pelo menos 4 caracteres")
	@Pattern(regexp = "(?=\\S+$).+", message = "Senha não deve conter espaços em branco")
	private String senha;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSenha() {
		return senha;
	}

	public AuthDTO() {
		super();
	}
}

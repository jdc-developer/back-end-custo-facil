package com.custofacil.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.custofacil.domain.enums.UserRole;

@Entity
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String nome;
	
	@Column(unique=true)
	private String username;
	private String senha;
	private Integer role;
	
	@ManyToOne
	@JoinColumn(name = "empresa_id")
	private Empresa empresa;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Integer getRole() {
		return role;
	}
	
	public UserRole getRoleEnum() {
		return UserRole.toEnum(role);
	}

	public void setRole(UserRole role) {
		this.role = role.getCod();
	}

	public Usuario(Integer id, String nome, String username, String senha, Empresa empresa, UserRole role) {
		super();
		this.id = id;
		this.nome = nome;
		this.username = username;
		this.senha = senha;
		this.empresa = empresa;
		this.role = role.getCod();
	}

	public Usuario() {
		super();
	}

}

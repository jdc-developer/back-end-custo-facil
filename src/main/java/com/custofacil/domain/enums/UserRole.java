package com.custofacil.domain.enums;

public enum UserRole {

	ADMIN(1, "ROLE_ADMIN"),
	COMMON(2, "ROLE_COMMON");
	
	private Integer cod;
	private String descricao;
	
	public Integer getCod() {
		return cod;
	}
	public void setCod(Integer cod) {
		this.cod = cod;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	private UserRole(Integer cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}
	
	public static UserRole toEnum(Integer cod) {
		if (cod == null) {
			return null;
		}
		
		for(UserRole x : UserRole.values()) {
			if(cod.equals(x.getCod())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Id inválido: " + cod);
	}
}

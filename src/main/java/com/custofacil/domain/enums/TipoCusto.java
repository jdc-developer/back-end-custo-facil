package com.custofacil.domain.enums;

public enum TipoCusto {
	
	MANUTENCAO(1, "Manutenção"),
	COMIDA(2, "Comida"),
	ESTADIA(3, "Estadia"),
	OUTROS(4, "Outros");
	
	private Integer cod;
	private String descricao;
	
	public Integer getCod() {
		return cod;
	}
	public void setCod(Integer cod) {
		this.cod = cod;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	private TipoCusto(Integer cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}
	
	public static TipoCusto toEnum(Integer cod) {
		if (cod == null) {
			return null;
		}
		
		for(TipoCusto x : TipoCusto.values()) {
			if(cod.equals(x.getCod())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Id inválido: " + cod);
	}

}

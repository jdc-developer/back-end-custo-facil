package com.custofacil.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.custofacil.domain.enums.TipoCusto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Custo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private Integer tipo;
	private double valor;
	private Date dtCusto;
	private String observacao;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "empresa_id")
	private Empresa empresa;
	
	@ManyToOne
	@JoinColumn(name = "veiculo_id")
	private Veiculo veiculo;
	
	@ManyToOne
	@JoinColumn(name = "motorista_id")
	private Motorista motorista;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTipo() {
		return tipo;
	}
	
	public TipoCusto getTipoEnum() {
		return TipoCusto.toEnum(tipo);
	}

	public void setTipo(TipoCusto tipo) {
		this.tipo = tipo.getCod();
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Date getDtCusto() {
		return dtCusto;
	}

	public void setDtCusto(Date dtCusto) {
		this.dtCusto = dtCusto;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public Motorista getMotorista() {
		return motorista;
	}

	public void setMotorista(Motorista motorista) {
		this.motorista = motorista;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Custo(Integer id, Integer tipo, double valor, Date dtCusto, Empresa empresa, Veiculo veiculo,
			Motorista motorista, String observacao) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.valor = valor;
		this.dtCusto = dtCusto;
		this.empresa = empresa;
		this.veiculo = veiculo;
		this.motorista = motorista;
		this.observacao = observacao;
	}

	public Custo() {
		super();
	}

}
